const user = require("../src/user");

it("ID 1", () => {
 expect(user(1)).toMatchSnapshot();
});
it("ID 56", () => {
 expect(user(56)).toMatchSnapshot();
});
it("ID 1345", () => {
 expect(user(1345)).toMatchSnapshot();
});
it("throws error if string", () => {
 expect(() => {
  user("a");
 }).toThrow();
});
